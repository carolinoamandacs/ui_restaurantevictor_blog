$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,		    
	    
	});
	
	
		
});